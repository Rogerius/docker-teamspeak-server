FROM registry.gitlab.com/rogerius/alpine-glibc:latest

MAINTAINER Ádám Keserű <keseru.adam@gmail.com>

RUN set -xe \
  # Installing build dependencies
  && apk add --no-cache --virtual=.build-dependencies ca-certificates wget w3m \
  # Downloading base archive
  && TS_SERVER_VER="$(w3m -dump https://www.teamspeak.com/downloads | grep -m 1 'Server 64-bit ' | awk '{print $NF}')" \
  && wget http://dl.4players.de/ts/releases/${TS_SERVER_VER}/teamspeak3-server_linux_amd64-${TS_SERVER_VER}.tar.bz2 -O /tmp/teamspeak.tar.bz2 \
  # Extracting base archive to /srv
  && tar xjf /tmp/teamspeak.tar.bz2 -C /srv \
  # Renaming to 'teamspeak'
  && mv /srv/teamspeak3-server_* /srv/teamspeak \
  # Removing build dependencies
  && rm /tmp/teamspeak.tar.bz2 \
  && apk del --no-cache .build-dependencies

# Accounting port (TCP)
EXPOSE 2008/tcp
# Weblist port (UDP)
EXPOSE 2010/udp
# Default weblist port (UDP out)
EXPOSE 2011/udp
# Default voice port (UDP in)
EXPOSE 9987/udp
# Default serverquery port (TCP in)
EXPOSE 10011/tcp
# Default filetransfer port (TCP in)
EXPOSE 30033/tcp
# Default tsdns port (TCP in)
EXPOSE 41144/tcp

VOLUME /srv/teamspeak/files/ /srv/teamspeak/logs/ /srv/teamspeak/ts3server.sqlitedb

ENTRYPOINT ["/srv/teamspeak/ts3server_minimal_runscript.sh"]
