Guide from: https://blog.akpwebdesign.com/2016/08/11/migrating-services-to-docker-pt-1/

Usage:
docker run -d --restart=always --name teamspeak3 \  
    -v /srv/teamspeak/files/:/srv/teamspeak/files/ \
    -v /srv/teamspeak/logs/:/srv/teamspeak/logs/ \
    -v /srv/teamspeak/ts3server.sqlitedb:/srv/teamspeak/ts3server.sqlitedb \
    -p 2008:2008/tcp -p 2010:2010/udp -p 2011:2011/udp \
    -p 9987:9987/udp -p 10011:10011/tcp -p 30033:30033/tcp \
    registry.gitlab.com/rogerius/teamspeak-docker-server
